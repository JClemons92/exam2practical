// Exam 2 Practical
// Jason Clemons

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>
using namespace std;

// declarations
void gatherInput(float *iArray);
void printInput(float *iArray);
int FindAvg(float *iArray);
int FindMin(float *iArray);
int FindMax(float *iArray);
void saveInputs(float *iArray);

int main()
{
	// variables
	float inputs[5];

	// methods
	gatherInput(inputs);
	printInput(inputs);
	cout << FindAvg(inputs) << "\n" << "\n";
	cout << FindMin(inputs) << "\n" << "\n";
	cout << FindMax(inputs) << "\n" << "\n";
	saveInputs(inputs);
	
	// console hold-open
	(void)_getch();
	return 0;
}

void gatherInput(float *iArray) {

	// loop through and gather input
	for (int i = 0; i < 5; i++) {

		// prompt user for input, and store input into array
		cout << "input a number: ";
		cin >> iArray[i];
	}

	// housekeeping
	cout << "\n";
}

void printInput(float *iArray) {

	// print out prefix
	cout << "Input numbers are : ";

	// loop through and print content and inputs
	for (int i = 0; i < 5; i++) {

		// print inputs, comma seporated
		if (i != 4) {cout << iArray[i] << ", ";}
		else {cout << iArray[i];}
	}

	// housekeeping
	cout << "\n" << "\n";
}

int FindMin(float *iArray) {

	// print out prefix
	cout << "Min of input numbers is: ";

	// loop through and find min
	float min = iArray[0];
	for (int i = 0; i < 5; i++) {
		
		if (i != 0) {
			if (iArray[i] < iArray[i - 1]) { min = iArray[i]; }
		}
		else {min = iArray[i];}
	}

	// return max
	return min;
}

int FindMax(float *iArray) {

	// print out prefix
	cout << "Max of input numbers is: ";

	// loop through and find max
	float max = iArray[0];
	for (int i = 0; i < 5; i++) {
		if (i != 0) {
			if (iArray[i] > iArray[i-1]) { max = iArray[i]; }
		}
		else {max = iArray[i];}
	}

	// return max
	return max;
}

int FindAvg(float *iArray) {

	// print out prefix
	cout << "Average of input numbers is: ";

	// loop through and print content and inputs
	float sum = 0;
	float avg = 0;
	for (int i = 0; i < 5; i++) {
		sum = sum + iArray[i];
		avg = sum / 5;
	}

	// return avg
	return avg;
}

void saveInputs(float *iArray) {
	char saverno;

	//prompt user to save a file
	cout << "Would you like to save output to file (y/n):";
	cin >> saverno;

	//create file if needed
	if ((saverno == 'y') || (saverno == 'Y')) {

		// set filepath
		string filePath = "Numbers.txt";

		// open and write to file
		ofstream ofs(filePath); // error check?
		if (ofs) {
			for (int i = 0; i < 5; i++) {
				// print inputs to page, comma seporated
				if (i != 4) {
					ofs << iArray[i] << ", ";
				}
				else {
					ofs << iArray[i];
				}
			}
		}

		// close file
		ofs.close();
	}
}